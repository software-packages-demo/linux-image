# linux-image

Linux kernel image and core modules. https://kernel.org

[[_TOC_]]

# Recommanded hardware
* [*CheapServerBoxHardware*](https://wiki.debian.org/CheapServerBoxHardware)

# Filesystems
* [DrivesAndPartitions](https://help.ubuntu.com/community/DrivesAndPartitions)

## BTRFS
### Official documentation
* [btrfs.wiki.kernel.org](https://btrfs.wiki.kernel.org/index.php/Main_Page)
* [*Manpage/btrfs(5)*](https://btrfs.wiki.kernel.org/index.php/Manpage/btrfs(5))
* [*Restore*](https://btrfs.wiki.kernel.org/index.php/Restore)
* [*Btrfsck*](https://btrfs.wiki.kernel.org/index.php/Btrfsck)
  * [*You mostly don't need fsck on btrfs*
    ](http://marc.merlins.org/perso/btrfs/post_2014-03-19_Btrfs-Tips_-Btrfs-Scrub-and-Btrfs-Filesystem-Repair.html)
    2014-03 Marc MERLIN
* [*FAQ*](https://btrfs.wiki.kernel.org/index.php/FAQ)
* [*Problem FAQ*](https://btrfs.wiki.kernel.org/index.php/Problem_FAQ)

### Unofficial documentation
* [*How to recover a BTRFS partition*](https://ownyourbits.com/2019/03/03/how-to-recover-a-btrfs-partition/)
  2019-03 nachoparker
* [*How do I recover a BTRFS partition that will not mount?*
  ](https://askubuntu.com/questions/157917/how-do-i-recover-a-btrfs-partition-that-will-not-mount)

### Other debian packages for BTRFS
* [packages.debian.org/btrfs](https://packages.debian.org/btrfs)

## UFS, UFS2 (FreeBSD, etc.)
### Official documentation
* [*Using UFS*](https://www.kernel.org/doc/html/latest/admin-guide/ufs.html)
  (The Linux Kernel)

### Unofficial documentation
* [linux ufs ufstype ufs2 read write
  ](https:/google.com/search?q=linux+ufs+ufstype+ufs2+read+write)
* [*How to enable Unix file system support on Linux kernel? [duplicate]*
  ](https://unix.stackexchange.com/questions/309014/how-to-enable-unix-file-system-support-on-linux-kernel)
  (2016)
* [*Mounting ufs partition with read/write permissions on ubuntu 10.04*
  ](https://unix.stackexchange.com/questions/24589/mounting-ufs-partition-with-read-write-permissions-on-ubuntu-10-04)
  (2015)
* [*Linux: Mount FreeBSD UFS 2 File System Command*
  ](https://www.cyberciti.biz/faq/howto-linux-mount-freebsd-ufs2-filesystem/)
  2012-07 Vivek Gite
* [*Linux Disable Mounting of Uncommon Filesystem*
  ](https://www.cyberciti.biz/faq/linux-disable-mounting-of-uncommon-filesystem/)
  2009-11 Vivek Gite

# Networking
## Ethernet
## Wifi
* [*WiFi*](https://wiki.debian.org/WiFi) (Debian with chipsets compatibility)
  (Look also at related pages in the Debian Wiki. They are listed at the end of the page!)
* (fr) [*Pas à pas : faire que sa carte Wi-Fi soit détectée et fonctionnelle*
  ](http://debian-facile.org/doc:materiel:wifi:detecter)
  2013-04 .. 2019-09

## Router
![nftables vs iptables popularity on Debian](https://qa.debian.org/cgi-bin/popcon-png?packages=nftables+python3-nftables+iptables&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=1&want_legend=1&want_ticks=1&from_date=2019-01-01&to_date=&hlght_date=&date_fmt=%25Y)

* [linux router configuration](https://google.com/search?q=linux+router+configuration)

### Books
* *Linux firewalls : enhancing security with nftables and beyond*
  2015 (Fourth edition) Steve Suehring (Addison-Wesley)
  * Ch. 4. nftables: The Linux Firewall Administration Program

### Unofficial documentation
* Check if about outdated iptables
* [*How to set up a NAT router on a Linux-based computer*
  ](https://how-to.fandom.com/wiki/How_to_set_up_a_NAT_router_on_a_Linux-based_computer)
  (2021) How To Wiki
* [*How to Configure and use Linux as a Router*
  ](https://www.computernetworkingnotes.com/linux-tutorials/how-to-configure-and-use-linux-as-a-router.html)
  (2021-06) ComputerNetworkingNotes
* [*Linux Set Up Routing with ip Command*
  ](https://www.cyberciti.biz/faq/howto-linux-configuring-default-route-with-ipcommand/)
  2018-12 Vivek Gite
* [*How To: Build a Simple Router with Ubuntu Server 18.04.1 LTS (Bionic Beaver)*
  ](https://www.ascinc.com/blog/linux/how-to-build-a-simple-router-with-ubuntu-server-18-04-1-lts-bionic-beaver/)
  2018-10 Blaz Valentinuzzi
* [*How to Turn a Linux Server into a Router to Handle Traffic Statically and Dynamically – Part 10*
  ](https://www.tecmint.com/setup-linux-as-router/)
  2015-01 (Updated 2018-09) Gabriel Cánepa
* [*LFCE: Installing Network Services and Configuring Automatic Startup at Boot – Part 1*
  ](https://www.tecmint.com/installing-network-services-and-configuring-services-at-system-boot/)
  2014-11 (Updated 2018-09) Gabriel Cánepa
* [*Setting up a Linux gateway/router, a guide for non network admins*
  ](https://monoinfinito.wordpress.com/series/setting-up-a-linux-gatewayrouter-a-guide-for-non-network-admins/)
  2014-12

#### Iptables (outdated)
* [*How To Setup an Iptables Firewall to Enable Remote Access to Services in Linux – Part 8*
  ](https://www.tecmint.com/configure-iptables-firewall/)
  2015-01 (Updated 2018-09) Gabriel Cánepa

### Alpine Linux
* [configuring alpine linux as router
  ](https://google.com/search?q=configuring+alpine+linux+as+router)
* [*Tutorials and Howtos*
  ](https://wiki.alpinelinux.org/wiki/Tutorials_and_Howtos)
* [*Linux Router with VPN on a Raspberry Pi*
  ](https://wiki.alpinelinux.org/wiki/Linux_Router_with_VPN_on_a_Raspberry_Pi)
* *The Router Project*
  * [*Part 2: Setting up alpine linux*
    ](https://oscillate.gr/words/router-pt2/)
  * [*Part 3: Router interface configuration*
    ](https://oscillate.gr/words/router-pt3/)
* [*Building A Network Gateway With Alpine Linux*
  ](https://medium.com/@privb0x23/airfield-altitude-building-a-network-gateway-with-alpine-linux-454a56457d53)
  2017-07 privb0x23

### OpenWrt
* [configuring openwrt](https://google.com/search?q=configuring+openwrt)
* [*Quick start guide for OpenWrt installation*
  ](https://openwrt.org/docs/guide-quick-start/start)
* [*Factory install: First-time installation on a device*
  ](https://openwrt.org/docs/guide-quick-start/factory_installation)

## Wifi Access-point
* hostapd
* network-manager
* [*Wireless access point*](https://en.m.wikipedia.org/wiki/Wireless_access_point) (Wikipedia)
* [*How to setup an Access Point mode Wi-Fi Hotspot?*
  ](https://askubuntu.com/questions/180733/how-to-setup-an-access-point-mode-wi-fi-hotspot)
  (2019)

### Harware capability
* iw
* [*linux how to know if wifi supports access point mode*
  ](https://google.com/search?q=linux+how+to+know+if+wifi+supports+access+point+mode)
* [*how to find out if my card supports infrastructure mode?*
  ](https://askubuntu.com/questions/106745/how-to-find-out-if-my-card-supports-infrastructure-mode)
  (2012)
* [*How can I tell if my wifi dongle has access-point capabilities?*
  ](https://unix.stackexchange.com/questions/92727/how-can-i-tell-if-my-wifi-dongle-has-access-point-capabilities)
  (2014)

### Alpine Linux
* [*How to setup a wireless access point*
  ](https://wiki.alpinelinux.org/wiki/How_to_setup_a_wireless_access_point)

### OpenWrt
* [*Enabling a Wi-Fi access point on OpenWrt*
  ](https://openwrt.org/docs/guide-quick-start/basic_wifi)

# Database for computer hardware compatibility
* [deviwiki](https://google.com/search?q=deviwiki)
* [wikidevi](https://google.com/search?q=wikidevi)
* [*WikiDevi has gone offline on 31-10-2019*
  ](https://linustechtips.com/topic/1125969-minor-news-wikidevi-has-gone-offline-on-31-10-2019/)
  2019-10 Grand Admiral Thrawn
